package project.service;

import project.dao.UsersDAO;
import project.model.User;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import java.util.List;

@Stateless
public class UserService {

    @EJB
    private UsersDAO usersDAO;

    public List<User> getAll() {
        return usersDAO.getAll();
    }

    public void registryUser(User user) {
        usersDAO.registryUser(user);

    }

    public User findUserByLogin(String login) {
        return usersDAO.findUserByLogin(login);
    }

    public void deleteUserByLogin(String login) {
        usersDAO.findUserByLogin(login);

    }

    public void updateUser(String login, String password, String name, String surname, String role, String
            city, int birthYear) {
        usersDAO.updateUser(login, password, name, surname, role, city, birthYear);

    }
    public  void deleteActualUserAccount(String login) {
        usersDAO.deleteActualUserAccount(login);
    }
}

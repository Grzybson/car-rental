package project.service;

import project.dao.RentalDAO;
import project.model.Rental;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import java.util.Date;
import java.util.List;

@Stateless
public class RentalService {

    @EJB
    private RentalDAO rentalDAO;

    public List<Rental> getAllRentals() {
        return rentalDAO.getAllRentals();
    }

    public  void registryRental(Rental rental) {
        rentalDAO.registryRental(rental);
    }

    public  Rental findRentalById(Integer id) {
        return rentalDAO.findRentalById(id);
    }

    public void deleteRentalById(Integer id) {
        rentalDAO.deleteRentalById(id);
    }

    public void updateRental(Integer id, int userId, int carId, Date startDate, Date returnDate) {
        rentalDAO.updateRental(id, userId, carId, startDate, returnDate);

    }



}

package project.service;

import project.dao.CarDAO;
import project.model.Car;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import java.util.List;

@Stateless
public class CarService {

    @EJB
    private CarDAO carDAO;

    public List<Car> getAllCars() {
        return carDAO.getAllCars();
    }

    public void createCar(Car car) {
        carDAO.createCar(car);
    }

    public Car findCarById(Integer id) {
        return carDAO.findCarById(id);
    }

    public void deleteCarById(Integer id) {
        carDAO.deleteCarById(id);
    }

    public void updateCar(Integer id, String brand, String model, int year, String color,
                          String type, double power, double pricePerDay) {
        carDAO.updateCar(id, brand, model, year, color, type, power, pricePerDay);
    }
}

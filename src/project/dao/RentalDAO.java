package project.dao;

import project.db.DbConnector;
import project.model.Rental;
import javax.ejb.Stateless;
import java.sql.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Stateless
public class RentalDAO {


    public  List<Rental> getAllRentals() {
        List<Rental> rentals = new ArrayList<>();
        try {
            Connection connection = DbConnector.createConnection();
            PreparedStatement peso = connection.prepareStatement("SELECT * from rental;");
                ResultSet result = peso.executeQuery();
                while (result.next()) {
                    Rental rental = createRentalFromRs(result);
                    rentals.add(rental);

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return rentals;
    }

    public  void registryRental(Rental rental) {
        try {
            Connection c = DbConnector.createConnection();
            PreparedStatement ps = c.prepareStatement("INSERT into rental values(?,?,?,?,?);");
            ps.setInt(1, rental.getId());
            ps.setInt(2, rental.getCarId());
            ps.setString(3, rental.getUserId());
            ps.setDate(4, (java.sql.Date) rental.getStartDate());
            ps.setDate(5, (java.sql.Date) rental.getReturnDate());
            ps.executeUpdate();

        } catch (Exception w) {
            w.printStackTrace();
        }
    }

    public  Rental findRentalById(Integer id) {
        try {
            Connection c = DbConnector.createConnection();
            PreparedStatement ps = c.prepareStatement("SELECT * from rental where id = ?;");

            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                return createRentalFromRs(rs);

            }
        }catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public void deleteRentalById(Integer id) {
        try {
            Connection conn = DbConnector.createConnection();
            PreparedStatement ps = conn.prepareStatement("DELETE from rental where id = ?;");

            ps.setInt(1, id);
            ps.executeUpdate();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void updateRental(Integer id, int userId, int carId, Date startDate, Date returnDate) {
        try {
            Connection c = DbConnector.createConnection();
            PreparedStatement ps = c.prepareStatement(" UPDATE rental set user_id = ?, car_id = ?, start_date = ?, return_date = ? where id = ?;");
            ps.setInt(1, carId);
            ps.setInt(2, userId);
            ps.setDate(3, (java.sql.Date) startDate);
            ps.setDate(4, (java.sql.Date) returnDate);
            ps.setInt(5, id);
            ps.executeUpdate();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }



    private Rental createRentalFromRs(ResultSet resultSet) throws SQLException {

        Integer id = resultSet.getInt("id");
        String user_id = resultSet.getString("user_id");
        int car_id = resultSet.getInt("car_id");
        java.sql.Date start_date = resultSet.getDate("start_date");
        java.sql.Date return_date = resultSet.getDate("return_date");

        return new Rental(id, user_id, car_id, start_date, return_date);
    }
}




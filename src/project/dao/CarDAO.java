package project.dao;

import project.db.DbConnector;
import project.model.Car;
import javax.ejb.Stateless;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Stateless
public class CarDAO {


    public  List<Car> getAllCars() {
        List<Car> cars = new ArrayList<>();
        try{
            Connection c = DbConnector.createConnection();
            PreparedStatement ps = c.prepareStatement("select * from car;");
            ResultSet resultSet = ps.executeQuery();

            while(resultSet.next()){
                Car carsFromRs = createCarFromRs(resultSet);
                cars.add(carsFromRs);

            }
        }
        catch (Exception e){
            e.printStackTrace();
        }
        return cars;
    }

    public  void createCar(Car car) {
        try {
            Connection c = DbConnector.createConnection();
            PreparedStatement ps = c.prepareStatement("Insert into car values(?,?,?,?,?,?,?,?);");
            ps.setInt(1, car.getId());
            ps.setString(2, car.getBrand());
            ps.setString(3, car.getModel());
            ps.setInt(4, car.getYear());
            ps.setString(5, car.getColor());
            ps.setString(6, car.getType());
            ps.setDouble(7, car.getPower());
            ps.setDouble(8, car.getPricePerDay());
            ps.executeUpdate();
        }catch(Exception e){
            e.printStackTrace();
        }

    }


    public  Car findCarById(Integer id){
        try{
            Connection conn = DbConnector.createConnection();
            PreparedStatement preparedStatement = conn.prepareStatement("Select * from car where id = ?;");

            preparedStatement.setInt(1, id);
            ResultSet rs = preparedStatement.executeQuery();

            while (rs.next()){
                return createCarFromRs(rs);
            }

        }catch (Exception exception){
            exception.printStackTrace();
        }

        return null;
    }


    public  void deleteCarById(Integer id){
        try{
            Connection c = DbConnector.createConnection();
            PreparedStatement ps = c.prepareStatement("DELETE from car where id = ?;");

            ps.setInt(1, id);
            ps.executeUpdate();

        }catch (Exception e){
            e.printStackTrace();
        }

    }

    public void updateCar(Integer id, String brand, String model, int year, String color, String type, double power, double pricePerDay) {
        try{
            Connection c = DbConnector.createConnection();
            PreparedStatement ps = c.prepareStatement("UPDATE car set brand = ?, model = ?, year = ?, color = ?, type = ?, " +
                    "power = ?, price_per_day = ? where id = ?;");
            ps.setString(1, brand);
            ps.setString(2, model);
            ps.setInt(3, year);
            ps.setString(4, color);
            ps.setString(5, type);
            ps.setDouble(6, power);
            ps.setDouble(7, pricePerDay);
            ps.setInt(8, id);

            ps.executeUpdate();

        }catch (Exception e){
            e.printStackTrace();
        }

    }

    private  Car createCarFromRs(ResultSet rs) throws SQLException {

        Integer id = rs.getInt("id");
        String brand = rs.getString("brand");
        String model = rs.getString("model");
        int year = rs.getInt("year");
        String color = rs.getString("color");
        String type = rs.getString("type");
        double power = rs.getDouble("power");
        double price_per_day = rs.getDouble("price_per_day");

        return new Car(id, brand, model, year, color, type, power, price_per_day);
    }


}

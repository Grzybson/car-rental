package project.dao;

import project.db.DbConnector;
import project.model.User;

import javax.ejb.Stateless;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Stateless
public class UsersDAO {


    public List<User> getAll() {
        List<User> user = new ArrayList<>();
        try {
            Connection connection = DbConnector.createConnection();
            PreparedStatement ps = connection.prepareStatement("Select * from user;");
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                User userFromDb = createUserFromRs(rs);
                user.add(userFromDb);

            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return user;
    }

    public void registryUser(User user) {
        try {
            Connection connection = DbConnector.createConnection();
            PreparedStatement ps = connection.prepareStatement("Insert into user values (?, ?, ?, ?, ?, ?, ?);");

            ps.setString(1, user.getLogin());
            ps.setString(2, user.getPassword());
            ps.setString(3, user.getName());
            ps.setString(4, user.getSurname());
            ps.setString(5, user.getCity());
            ps.setString(6, user.getRole());
            ps.setInt(7, user.getBirthYear());
            ps.executeUpdate();

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public User findUserByLogin(String login) {
        try {
            Connection connection = DbConnector.createConnection();
            PreparedStatement ps = connection.prepareStatement("SELECT * from user where login = ?;");

            ps.setString(1, login);
            ResultSet rs = ps.executeQuery();

            if (rs.next()) {
                return createUserFromRs(rs);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    public void deleteUserByLogin(String login) {
        try {
            Connection connection = DbConnector.createConnection();
            PreparedStatement ps = connection.prepareStatement("Delete from user where login = ?;");
            ps.setString(1, login);

            ps.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void updateUser(String login, String password, String name, String surname, String role, String
            city, int birthYear) {
        try {
            Connection connection = DbConnector.createConnection();
            PreparedStatement ps = connection.prepareStatement("Update user set password = ?, name = ?," +
                    "surname = ?, city = ?, role = ?, birth_year = ? where login = ? ;");
            ps.setString(1, password);
            ps.setString(2, name);
            ps.setString(3, surname);
            ps.setString(4, city);
            ps.setString(5, role);
            ps.setInt(6, birthYear);
            ps.setString(7, login);

            ps.executeUpdate();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private User createUserFromRs(ResultSet rs) throws SQLException {
        String login = rs.getString("login");
        String password = rs.getString("password");
        String name = rs.getString("name");
        String surname = rs.getString("surname");
        String city = rs.getString("city");
        String role = rs.getString("role");
        int birth_year = rs.getInt("birth_year");
        return new User(login, password, name, surname, city, role, birth_year);
    }

    public  void deleteActualUserAccount(String login) {
        try {
            Connection connection = DbConnector.createConnection();
            PreparedStatement ps = connection.prepareStatement("Delete from user where login = ?;");
            ps.setString(1, login);

            ps.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        }
   }

}

package project.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DbConnector {

    public static Connection createConnection() throws ClassNotFoundException, SQLException, IllegalAccessException, InstantiationException {
        String url = "jdbc:mysql://localhost:3306/cars_rental?allowPublicKeyRetrieval=true&autoReconnect=true&useSSL=false&serverTimezone=UTC";
        String username = "root";
        String password = "root";
        Class.forName("com.mysql.jdbc.Driver").newInstance();
        return DriverManager.getConnection(url, username, password);

    }
}

package project.view;

import project.model.Rental;
import project.service.RentalService;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet("/rentalsList")
public class RentalsListServlet extends HttpServlet {

    @EJB
    private RentalService rentalService;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        List<Rental> allRental = rentalService.getAllRentals();

        req.setAttribute("rentals", allRental);
        req.getRequestDispatcher("rentals.jsp").forward(req, resp);

    }


}

package project.view;

import project.model.Car;
import project.service.CarService;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet("/carsList")
public class CarsListServlet extends HttpServlet {

    @EJB
    private CarService carService;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        List<Car> allCars = carService.getAllCars();

        req.setAttribute("cars", allCars);
        req.getRequestDispatcher("/cars.jsp").forward(req, resp);


    }
}

package project.servlets;

import project.db.DbConnector;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/testDB")
public class TestDbServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try{
            DbConnector.createConnection();
            resp.getWriter().write("Connection succesful");
        }catch(Exception e){
            resp.getWriter().write("Connection failed" + e.toString());
        }
    }
}

//http://localhost:8080/Cars_Rental_In_Home_war_exploded/testDB

package project.servlets.carServlets;

import project.dao.CarDAO;
import project.dao.RentalDAO;
import project.dao.UsersDAO;
import project.model.Car;
import project.model.Rental;
import project.model.User;
import project.service.CarService;
import project.service.RentalService;
import project.service.UserService;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Date;
import java.time.LocalDate;

@WebServlet("/rent")
public class RentCarServlet extends HttpServlet {

    @EJB
    private CarService carService;
    @EJB
    private UserService userService;
    @EJB
    private RentalService rentalService;

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException, IllegalArgumentException {

        String username = (String) req.getSession().getAttribute("username");
        int carId = Integer.parseInt(req.getParameter("carId"));
        Car rentingCar = carService.findCarById(carId);
        User user = userService.findUserByLogin(username);

        if (user != null && rentingCar != null) {
            Integer id = Integer.valueOf(req.getParameter("id"));

            java.sql.Date startDate = Date.valueOf(req.getParameter("startDate"));
            java.sql.Date returnDate = Date.valueOf(req.getParameter("returnDate"));

            rentalService.registryRental(new Rental(id, user.getLogin(), carId, startDate, returnDate));
            resp.sendRedirect(resp.encodeRedirectURL(req.getContextPath() + "/rentals.jsp"));
        } else {
            resp.sendRedirect(resp.encodeRedirectURL(req.getContextPath() + "/cars.jsp"));
        }
    }
}


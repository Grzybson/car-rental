package project.servlets.carServlets;

import project.dao.CarDAO;
import project.service.CarService;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/editCar")
public class EditCarServlet extends HttpServlet {

    @EJB
    private CarService carService;

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        Integer id = Integer.valueOf(req.getParameter("id"));
        String brand = req.getParameter("brand");
        String model = req.getParameter("model");
        int year = Integer.parseInt(req.getParameter("year"));
        String color = req.getParameter("color");
        String type = req.getParameter("type");
        double power = Double.parseDouble(req.getParameter("power"));
        double pricePerDay = Double.parseDouble(req.getParameter("pricePerDay"));

        carService.updateCar(id, brand, model, year, color, type, power, pricePerDay);
        resp.sendRedirect(resp.encodeRedirectURL(req.getContextPath() + "/cars.jsp"));

    }
}

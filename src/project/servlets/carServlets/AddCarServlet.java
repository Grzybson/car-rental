package project.servlets.carServlets;


import project.model.Car;
import project.service.CarService;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/addCar")
public class AddCarServlet extends HttpServlet {

    @EJB
    private CarService carService;


    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html;charset=UTF-8");

        String idInUse = req.getParameter("thatIdIsInUse");

        resp.getWriter().write("<!DOCTYPE html>\n" +
                "<html lang=\"en\">\n" +
                "<head>\n" +
                "    <meta charset=\"UTF-8\">\n" +
                "    <title>Car_adding</title>\n" +
                "</head>\n");
        if (idInUse != null) {
            resp.getWriter().write("CAR WITH THIS ID EXIST");
        }
        resp.getWriter().write("<body>\n" +
                "<h1>Car Added page</h1>\n" +
                "<h3>Fill in some information in empty brackets</h3>\n" +
                "<label>\n" +
                "    <form method=\"post\" action=\"addCar\">\n" +
                "        id\n" +
                "        <input type=\"number\" name=\"id\">\n" +
                "        <br>\n" +
                "        brand\n" +
                "        <input type=\"text\" name=\"brand\">\n" +
                "        <br>\n" +
                "        model\n" +
                "        <input type=\"text\" name=\"model\">\n" +
                "        <br>\n" +
                "        year\n" +
                "        <input type=\"number\" name=\"year\">\n" +
                "        <br>\n" +
                "        color\n" +
                "        <input type=\"color\" name=\"color\">\n" +
                "        <br>\n" +
                "        type\n" +
                "        <input type=\"text\" name=\"type\">\n" +
                "        <br>\n" +
                "        power\n" +
                "        <input type=\"number\" name=\"power\">\n" +
                "        <br>\n" +
                "        pricePerDay\n" +
                "        <input type=\"number\" name=\"pricePerDay\">\n" +
                "        <br>\n" +
                "        add\n" +
                "        <input type=\"submit\" value=\"add\">" +
                "\n" +
                "    </form>\n" +
                "</label>\n" +
                "\n" +
                "</body>\n" +
                "</html>");
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        Integer idParam = Integer.valueOf(req.getParameter("id"));
        Car existingCar = carService.findCarById(idParam);
        if (existingCar != null) {
            resp.sendRedirect(resp.encodeRedirectURL(req.getContextPath() + "/addCar?thatIdIsInUse"));
        } else {
            Integer id = Integer.valueOf(req.getParameter("id"));
            String brand = req.getParameter("brand");
            String model = req.getParameter("model");
            int year = Integer.parseInt(req.getParameter("year"));
            String color = req.getParameter("color");
            String type = req.getParameter("type");
            double power = Double.parseDouble(req.getParameter("power"));
            double pricePerDay = Double.parseDouble(req.getParameter("pricePerDay"));

            carService.createCar(new Car(id, brand, model, year, color, type, power, pricePerDay));
            resp.sendRedirect(resp.encodeRedirectURL(req.getContextPath() + "/cars.jsp"));
        }
    }
}

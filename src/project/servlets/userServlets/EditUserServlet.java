package project.servlets.userServlets;


import project.service.UserService;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/editUser")
public class EditUserServlet extends HttpServlet {

    @EJB
    private UserService userService;


    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String login = req.getParameter("login");
        String password = req.getParameter("password");
        String name = req.getParameter("name");
        String surname = req.getParameter("surname");
        String city = req.getParameter("city");
        String role = req.getParameter("role");
        String birthYearString = req.getParameter("year");
        int birthYear = Integer.valueOf(birthYearString);


        userService.updateUser(login, password, name, surname, role, city, birthYear);
        resp.sendRedirect(resp.encodeRedirectURL(req.getContextPath() + "/users.jsp"));
    }

}


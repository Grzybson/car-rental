package project.servlets.userServlets;

import project.model.User;
import project.service.UserService;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

// http://localhost:8080/Cars_Rental_In_Home_war_exploded/login

@WebServlet("/login")
public class LoginServlet extends HttpServlet {

    @EJB
    private UserService userService;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html;charset=UTF-8");
        String invalidParam = req.getParameter("invalidLogin");

        resp.getWriter().write("<!DOCTYPE html>\n" +
                "<html lang=\"en\">\n" +
                "<head>\n" +
                "    <meta charset=\"UTF-8\">\n" +
                "    <title>Login formularz</title>\n" +
                "</head>\n" +
                "<body>\n");
        if (invalidParam != null) {
            resp.getWriter().write("Invalid login");
        }
        resp.getWriter().write("<h1>Welcome in login site</h1>\n" +
                "\n" +
                "<form action=\"login\" method=\"post\">\n" +
                "    <h3>Login:</h3>\n" +
                "        <input type=\"text\" name=\"login\">\n" +
                "    <h3>Password:</h3>\n" +
                "        <input type=\"password\" name=\"password\">\n" +
                "    <br>\n" +
                "    <input type=\"submit\" value=\"logowanie\">\n" +
                "</form>\n" +
                "<br><a href=\"register\">Go to register page</a>" +
                "</body>\n" +
                "</html>");
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String login = req.getParameter("login");
        String password = req.getParameter("password");
        User userWithLogin = userService.findUserByLogin(login);
        if (userWithLogin != null && userWithLogin.getPassword().equals(password)) {
            HttpSession session = req.getSession();
            session.setAttribute("username", login);
            resp.sendRedirect(resp.encodeRedirectURL(req.getContextPath() + "/Homepage.html"));
        } else {
            resp.sendRedirect(resp.encodeRedirectURL(req.getContextPath() + "/login?invalidLogin"));
        }
    }
}

package project.servlets.userServlets;


import project.model.User;
import project.service.UserService;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/register")
public class RegisterServlet extends HttpServlet {

    @EJB
    private UserService userService;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html;charset=UTF-8");

        String loginAlreadyExists = req.getParameter("loginAlreadyExists");

        resp.getWriter().write("<!DOCTYPE html>\n" +
                "<html lang=\"en\">\n" +
                "<head>\n" +
                "    <meta charset=\"UTF-8\">\n" +
                "    <title>Title</title>\n" +
                "</head>\n" +
                "<body>\n" +
                "\n" +
                "<h1>Register!</h1>\n");

        if (loginAlreadyExists != null) {
            resp.getWriter().write("USER WITH SPECIFIED LOGIN ALREADY EXISTS!");
        }
        resp.getWriter().write("<form action=\"register\" method=\"post\">\n" +
                "    <label>\n" +
                "        Login:\n" +
                "        <input type=\"text\" name=\"login\">\n" +
                "    </label>\n" +
                "    <br>\n" +
                "    <label>\n" +
                "        Password:\n" +
                "        <input type=\"password\" name=\"password\">\n" +
                "    </label>\n" +
                "    <br>\n" +
                "    <label>\n" +
                "        Name:\n" +
                "        <input type=\"text\" name=\"name\">\n" +
                "    </label>\n" +
                "    <br>\n" +
                "    <label>\n" +
                "        Surname:\n" +
                "        <input type=\"text\" name=\"surname\">\n" +
                "    </label>\n" +
                "    <br>\n" +
                "    <label>\n" +
                "        Role:\n" +
                "        <input name=\"role\" type=\"text\">\n" +
                "    </label>\n" +
                "    <br>\n" +
                "    <label>\n" +
                "        City:\n" +
                "        <input type=\"text\" name=\"city\">\n" +
                "    </label>\n" +
                "    <br>\n" +
                "    <label>\n" +
                "        Birth year:\n" +
                "        <input type=\"number\" name=\"birthYear\" value=\"1970\">\n" +
                "    </label>\n" +
                "    <br>\n" +
                "    <input type=\"submit\" value=\"Register!\">\n" +
                "</form>\n" +
                "<hr>\n" +
                "<a href=\"login\">Go to login page</a>\n" +
                "</body>\n" +
                "</html>\n");
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String login = req.getParameter("login");
        User existingUser = userService.findUserByLogin(login);
        if (existingUser != null) {
            resp.sendRedirect(resp.encodeRedirectURL("register?loginAlreadyExists"));
        } else {
            String password = req.getParameter("password");
            String name = req.getParameter("name");
            String surname = req.getParameter("surname");
            String role = req.getParameter("role");
            String city = req.getParameter("city");
            String birthYear = req.getParameter("birthYear");
            int birthYearAsNumber = Integer.parseInt(birthYear);

            User newUser = new User(login, password, name, surname, city,
                    role, birthYearAsNumber);
            userService.registryUser(newUser);

            resp.sendRedirect(resp.encodeRedirectURL("login"));
        }
    }
}



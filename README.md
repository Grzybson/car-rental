## About project:
(for the time being a pre-alpha version of app)
This is a Car rental application created only to show differences and compare it to my main project which is library app. 
Project is created with alternative technology and tools (JEE, JSP, JDBC, EJB, Servlets) 
as against (Spring MVC, Hibernate, JPA, Java 8, HTML, CSS, Thymeleaf) in library app

After login into application by our login & password, 
We can choose and rent car/s.

## Getting Started
First of all Instal MySQL and GlassFish serwer.

After `git clone` from repository make empty database in MySQL and named it e.g. car_rental 
or named otherwise but then fix datasource.url in properties.

##### How to create database:
* `create database car_rental`;
* `use car_rental`;

Another things to run application is to make properties in "Connector" class to connect
into our database.

#### Properties:
* String url = "jdbc:mysql://localhost:3306/cars_rental?allowPublicKeyRetrieval=true&autoReconnect=true&useSSL=false&serverTimezone=UTC"
* String username = "root";
* String password = "root";
* Class.forName("com.mysql.jdbc.Driver").newInstance();
* return DriverManager.getConnection(url, username, password);

##### Now app is ready to run.

## How to run application
After project setup, search main page: http://localhost:8080/Cars_Rental_In_Home_war_exploded/login
Register yourself or log into app as admin - login:admin, password:admin

## Technologies
Project is created with:
#### IDE
* IntelliJ IDEA
#### Languages       
* Java EE
* HTML
* CSS
#### Tools
* Servlets
* JSP
* JDBC
* EJB
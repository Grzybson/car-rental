<%@ page import="project.model.Rental" %>
<%@ page import="java.util.List" %>
<%@ page import="project.dao.RentalDAO" %>
<%@ page import="project.model.User" %>
<%@ page import="javax.jws.soap.SOAPBinding" %><%--
  Created by IntelliJ IDEA.
  User: Bartosz Grzyb
  Date: 18.08.2019
  Time: 10:59
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
    <style>
        h1 {
            text-align: center;
        }

        th {
            color: black;
        }

        td {
            text-align: center;
            color: darkblue;
        }

        table {
            width: 100%;
            border-collapse: collapse;
        }

        th, td {
            border: 4px solid black;
        }

    </style>
</head>
<body>
<h1>Rentals Table</h1>

<table>
    <tr>

        <th>id</th>
        <th>userid</th>
        <th>carid</th>
        <th>startDate</th>
        <th>returnDate</th>

    </tr>
    <%

        List<Rental> rentals = (List<Rental>) request.getAttribute("rentals");
        for(Rental rental : rentals){
    %>
    <tr>
        <td><%=rental.getId()%></td>
        <td><%=rental.getUserId()%></td>
        <td><%=rental.getCarId()%></td>
        <td><%=rental.getStartDate()%></td>
        <td><%=rental.getReturnDate()%></td>

    </tr>
    <%
        }
    %>
</table>
<br>
<h3><a href="Homepage.html">Homepage</a></h3>
</body>
</html>

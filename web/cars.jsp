<%@ page import="project.model.Car" %>
<%@ page import="java.util.List" %>
<%@ page import="project.dao.CarDAO" %><%--
  Created by IntelliJ IDEA.
  User: Bartosz Grzyb
  Date: 11.08.2019
  Time: 13:15
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<style>
    h1 {
        text-align: center;
        font-weight: 100;
    }

    td {
        text-align: center;
        color: darkblue;
    }
    table{
        width: 100%;
        border-collapse: collapse;
    }

    th {
        color: black;
    }

    td, th {
        border: 4px solid black;
    }
</style>
<body>
<h1>Cars Table</h1>

<table>
    <tr>
        <th>id</th>
        <th>brand</th>
        <th>model</th>
        <th>year</th>
        <th>color</th>
        <th>type</th>
        <th>power</th>
        <th>pricePerDay</th>
    </tr>
    <%
        List<Car> cars = (List<Car>) request.getAttribute("cars");
        for(Car car: cars){
    %>
    <tr>
        <td><%=car.getId()%></td>
        <td><%=car.getBrand()%></td>
        <td><%=car.getModel()%></td>
        <td><%=car.getYear()%></td>
        <td><%=car.getColor()%></td>
        <td><%=car.getType()%></td>
        <td><%=car.getPower()%></td>
        <td><%=car.getPricePerDay()%></td>
        <td><a href="rentCreate.jsp?id=<%=car.getId()%>">RENT</a></td>
        <td><a href="editCar.jsp?id=<%=car.getId()%>">Edit Car</a></td>
        <td><a href="deleteCar?id=<%=car.getId()%>">Delete Car</a></td>
    </tr>
    <%
        }
    %>
</table>
<br>
<h3>Add car to Car Table</h3>
<a href="CarAddForm.html">addCar</a>
<br>
<h4>Return to Homepage<a href="Homepage.html">Homepage</a></h4>
</body>
</html>


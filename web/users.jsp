<%@ page import="project.dao.UsersDAO" %>
<%@ page import="project.model.User" %>
<%@ page import="java.util.List" %><%--
  Created by IntelliJ IDEA.
  User: Bartosz Grzyb
  Date: 10.08.2019
  Time: 13:41
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
<style>
    h1{
        text-align: center;
    }
    table{
        width: 100%;
        border-collapse: collapse;
    }
    td{
        text-align: center;
        color: darkblue;
    }
    th{
        color: black;
    }
    td, th {
        border: 4px solid black;
    }
</style>
</head>
<body>

<h1>Users Table</h1>

<table>
    <tr>
        <th>login</th>
        <th>password</th>
        <th>name</th>
        <th>surname</th>
        <th>city</th>
        <th>role</th>
        <th>birthYear</th>
        <th>delete</th>
        <Th>update</Th>

    </tr>

<%
    List<User> users = (List<User>) request.getAttribute("users");
    for(User user: users) {
 %>
    <tr>
        <td><%=user.getLogin()%></td>
        <td><%=user.getPassword()%></td>
        <td><%=user.getName()%></td>
        <td><%=user.getSurname()%></td>
        <td><%=user.getCity()%></td>
        <td><%=user.getRole()%></td>
        <td><%=user.getBirthYear()%></td>
        <td><a href="deleteUser?login=<%=user.getLogin()%>">delete</a></td>
        <td><a href="editUser.jsp?login=<%=user.getLogin()%>">edit</a></td>
    </tr>
<%
    }
%>

</table>
<br>
<h4>Return to Homepage<a href="Homepage.html">Homepage</a> </h4>

</body>
</html>

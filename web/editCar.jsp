<%@ page import="project.model.Car" %>
<%@ page import="project.dao.CarDAO" %><%--
  Created by IntelliJ IDEA.
  User: Bartosz Grzyb
  Date: 18.08.2019
  Time: 15:37
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<%
    Integer id = Integer.valueOf(request.getParameter("id"));
    Car carToEdit = CarDAO.findCarById(id);
    if(carToEdit != null){
%>
<label>
    <form method="post" action="editCar">
        id
        <input type="number" name="id" value="<%=carToEdit.getId()%>">
        brand
        <input type="text" name="brand" value="<%=carToEdit.getBrand()%>">
        model
        <input type="text" name="model" value="<%=carToEdit.getModel()%>">
        year
        <input type="number" name="year" value="<%=carToEdit.getYear()%>">
        color
        <input type="color" name="color" value="<%=carToEdit.getColor()%>">
        type
        <input type="text" name="type" value="<%=carToEdit.getType()%>">
        power
        <input type="number" name="power" value="<%=carToEdit.getPower()%>">
        price per day
        <input type="number" name="pricePerDay" value="<%=carToEdit.getPricePerDay()%>">

        <input type="submit" value="sendToEdit">
    </form>
</label>
<%
    }
%>

</body>
</html>
